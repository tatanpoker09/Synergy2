package com.aletheia.mc.synergy.event;

public enum SynergyStatus {
	PRE_START,
	STARTED,
	VOTE,
	ENDED;
}
