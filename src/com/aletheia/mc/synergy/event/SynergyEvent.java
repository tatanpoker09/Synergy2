package com.aletheia.mc.synergy.event;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import me.confuser.barapi.BarAPI;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;

import com.aletheia.mc.synergy.Synergy;
import com.aletheia.mc.synergy.utils.Timer;
import com.worldcretornica.plotme_core.Plot;
import com.worldcretornica.plotme_core.bukkit.api.BukkitWorld;

public class SynergyEvent {

	private World world;
	private Map<Player, Plot> playerVote = new HashMap<Player, Plot>();
	private Map<Plot, Integer> plotVotes = new HashMap<Plot, Integer>();
	private static SynergyStatus status;
	private static List<SynergyEvent> events = new ArrayList<SynergyEvent>();
	private Scoreboard board = Bukkit.getScoreboardManager().getNewScoreboard();
	private List<Plot> topPlots = new ArrayList<Plot>();

	public SynergyEvent(World world){
		this.setWorld(world);
	}
	public static void start(int days, int hours){
		changeStatus(SynergyStatus.STARTED);
		for(World world : Bukkit.getWorlds()){
			if(Synergy.getPlotAPI().isPlotWorld(new BukkitWorld(world))) new SynergyEvent(world).add();
		}
		Bukkit.broadcastMessage(ChatColor.GREEN+"Started new synergy event!");
		Timer.startCountdown(days, hours);
	}
	public void add() {
		events.add(this);
	}
	public static void end(){
		Timer.cancel();
		changeStatus(SynergyStatus.ENDED);
		Bukkit.broadcastMessage(ChatColor.RED+"Ended synergy event!");
	}
	public static void restart(){
		Timer.cancel();
		events.clear();
		changeStatus(SynergyStatus.PRE_START);
		Bukkit.broadcastMessage(ChatColor.GREEN+"Synergy event has been restarted!");
	}

	public static void changeStatus(SynergyStatus status) {
		SynergyEvent.status = status;
		if(status.equals(SynergyStatus.PRE_START) || status.equals(SynergyStatus.ENDED)){
			BarAPI.setMessage(ChatColor.GREEN+"Status: "+ChatColor.GOLD+status.toString());
		}
	}

	public static SynergyStatus getStatus() {
		return status;
	}
	public World getWorld() {
		return world;
	}
	public void setWorld(World world) {
		this.world = world;
	}

	public Map<Player, Plot> getPlayerVote() {
		return playerVote;
	}
	public Map<Plot, Integer> getPlotVotes() {
		return plotVotes;
	}
	public static List<SynergyEvent> getEvents() {
		return events;
	}
	public static void setEvents(List<SynergyEvent> events) {
		SynergyEvent.events = events;
	}
	public static SynergyEvent getInstance(Player player) {
		for(SynergyEvent synEvent : getEvents()){
			if(synEvent.getWorld().equals(player.getWorld())){
				return synEvent;
			}
		}
		return null;
	}
	public static void updateScoreboard() {
		for(SynergyEvent synEvent : SynergyEvent.getEvents()){
			synEvent.loadTopPlots();
			for(Objective objective : synEvent.getBoard().getObjectives()) objective.unregister();
			Objective objective = synEvent.getBoard().registerNewObjective("Top Plots", "dummy");
			if(synEvent.getTopPlots().size()==0){
				objective.getScore(ChatColor.RED+"No plots").setScore(1);
				objective.getScore(ChatColor.RED+"with votes").setScore(0);
				return;
			}
			for(Plot plot: synEvent.getTopPlots()){
				int votes = synEvent.getPlotVotes().get(plot);
				objective.getScore(plot.getOwner()).setScore(votes);
			}
		}
	}
	private void loadTopPlots() {
		for(int x = 0;x<5;x++){
			Plot highest = null;
			int highestVote = 0;
			for(Plot plot : getPlotVotes().keySet()){
				if(!topPlots.contains(plot)){
					int plotVotes = getPlotVotes().get(plot);
					if(plotVotes>highestVote){
						highest = plot;
						highestVote = plotVotes;
					}
				}
			}
			if(highest!=null)topPlots.add(highest);
		}
	}
	private List<Plot> getTopPlots() {
		return topPlots ;
	}
	public Scoreboard getBoard() {
		return board;
	}
	public void setBoard(Scoreboard board) {
		this.board = board;
	}
}
