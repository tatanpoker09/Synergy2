package com.aletheia.mc.synergy;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import com.aletheia.mc.synergy.commands.SynergyCmd;
import com.aletheia.mc.synergy.event.SynergyEvent;
import com.aletheia.mc.synergy.event.SynergyStatus;
import com.aletheia.mc.synergy.utils.Timer;
import com.worldcretornica.plotme_core.PlotMeCoreManager;
import com.worldcretornica.plotme_core.bukkit.PlotMe_CorePlugin;

public final class Synergy extends JavaPlugin{
	private static JavaPlugin plugin;
	private static PlotMeCoreManager plotAPI;
	
	
	@Override
	public void onEnable(){
		PluginManager pm = Bukkit.getPluginManager();
		if(!pm.getPlugin("PlotMe").isEnabled()){
			Bukkit.getLogger().info("ERROR: PLOTME NOT INSTALLED");
			Bukkit.getLogger().info("DISABLING PLUGIN");
			pm.disablePlugin(this);
			return;
		}
		PlotMe_CorePlugin plotMePlugin = (PlotMe_CorePlugin)pm.getPlugin("PlotMe");
		plotAPI = new PlotMeCoreManager(plotMePlugin.getAPI());
		getCommand("synergy").setExecutor(new SynergyCmd());
		
		if(SynergyEvent.getStatus()==null){
			SynergyEvent.changeStatus(SynergyStatus.PRE_START);
		} else {
			loadFromConfig();
		}
	}
	
	@Override
	public void onDisable(){
		saveToConfig();
	}
	
	
	private void saveToConfig() {
		String status = SynergyEvent.getStatus().toString();
		getConfig().set("status", status);
		List<String> worlds = new ArrayList<String>();
		for(SynergyEvent synEvent : SynergyEvent.getEvents()){
			String worldName = synEvent.getWorld().toString();
			worlds.add(worldName);
		}
		getConfig().set("worlds", worlds);
		
		getConfig().set("Timer.days", Timer.getDays());
		getConfig().set("Timer.hours", Timer.getHours());
	}

	private void loadFromConfig() {
		String status = getConfig().getString("status");
		SynergyEvent.changeStatus(SynergyStatus.valueOf(status));
		
		List<String> worlds = getConfig().getStringList("worlds");
		for(String world : worlds){
			new SynergyEvent(Bukkit.getWorld(world)).add();
		}
		
		String days = getConfig().getString("Timer.days");
		String hours = getConfig().getString("Timer.hours");
		if(days!=""){
			if(hours!=""){
				Timer.startCountdown(Integer.parseInt(days), Integer.parseInt(hours));
			}
		}
	}
	
	
	public static JavaPlugin getPlugin() {
		return plugin;
	}

	public static PlotMeCoreManager getPlotAPI() {
		return plotAPI;
	}
}
