package com.aletheia.mc.synergy.utils;



import me.confuser.barapi.BarAPI;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.Wool;

import com.aletheia.mc.synergy.Synergy;
import com.aletheia.mc.synergy.event.SynergyEvent;
import com.aletheia.mc.synergy.event.SynergyStatus;

public class Timer {
	private static int taskToCancel;
	private static int hours;
	private static int days;
	private static int total;
	

	public static void startCountdown(int days, int hours) {
		BarAPI.setMessage(ChatColor.GREEN+"Time remaining: "+ChatColor.GOLD+Timer.getDays()+" days, "+Timer.getHours()+" hours. "+" Status: "+SynergyEvent.getStatus().toString());
		total = (days*24)+hours;
		Bukkit.getScheduler().cancelTask(taskToCancel);
		Timer.setDays(days);
		Timer.setHours(hours);
		SynergyEvent.updateScoreboard();
		taskToCancel = Bukkit.getScheduler().scheduleSyncRepeatingTask(Synergy.getPlugin(), new Runnable() {
			@Override
			public void run() {
				SynergyStatus status = SynergyEvent.getStatus();
				if(Timer.getHours()==0){
					Timer.setDays(Timer.getDays() - 1);
					Timer.setHours(24);
				}
				Timer.setHours(Timer.getHours() - 1);
				SynergyEvent.updateScoreboard();
				int current = (Timer.getDays()*24)+Timer.getHours();
				float percent = (current*100)/total;
				BarAPI.setMessage(ChatColor.GREEN+"Time remaining: "+ChatColor.GOLD+Timer.getDays()+" days, "+Timer.getHours()+" hours. "+" Status: "+SynergyEvent.getStatus().toString(), percent);
				if(Timer.getHours()==0 && Timer.getDays()==0){
					cancel();
					if(status.equals(SynergyStatus.STARTED)){
						SynergyEvent.changeStatus(SynergyStatus.VOTE);
						loadPlayerInventories();
						startCountdown(3, 0);
					} else {
						SynergyEvent.end();
					}
				}
			}
		}, 20L, 20L);
	}

	public static void loadPlayerInventories() {
		for(SynergyEvent event : SynergyEvent.getEvents()){
			for(Player player : event.getWorld().getPlayers()){
				ItemStack item = new ItemStack(Material.BOOK);
				MetaUtils.addName(item, ChatColor.DARK_BLUE+"Top Plots");
				player.getInventory().setItem(0, item);
				item = new Wool(DyeColor.LIME).toItemStack(1);
				MetaUtils.addName(item, ChatColor.GREEN+"Vote!");
			}
		}
	}

	public static void cancel() {
		Bukkit.getScheduler().cancelTask(taskToCancel);
	}

	public static void setDays(int days) {
		Timer.days = days;
	}

	public static void setHours(int hours) {
		Timer.hours = hours;
	}
	public static int getHours() {
		return hours;
	}

	public static int getDays() {
		return days;
	}
}