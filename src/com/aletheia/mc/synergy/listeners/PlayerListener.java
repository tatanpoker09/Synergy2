package com.aletheia.mc.synergy.listeners;

import org.bukkit.ChatColor;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.Wool;

import com.aletheia.mc.synergy.Synergy;
import com.aletheia.mc.synergy.event.SynergyEvent;
import com.aletheia.mc.synergy.event.SynergyStatus;
import com.aletheia.mc.synergy.utils.MetaUtils;
import com.worldcretornica.plotme_core.Plot;
import com.worldcretornica.plotme_core.bukkit.api.BukkitPlayer;

public class PlayerListener implements Listener{
	@EventHandler(priority=EventPriority.HIGHEST)
	public void onPlayerInteract(PlayerInteractEvent event){
		if(SynergyEvent.getStatus().equals(SynergyStatus.VOTE)){
			Action action = event.getAction();
			if(action.equals(Action.RIGHT_CLICK_AIR) || action.equals(Action.RIGHT_CLICK_BLOCK)){
				Player player = event.getPlayer();
				ItemStack item = player.getItemInHand();
				String itemName = item.getItemMeta().getDisplayName();
				SynergyEvent instance = SynergyEvent.getInstance(player);
				if(itemName.equals(ChatColor.GREEN+"Vote!")){
					Plot plot = Synergy.getPlotAPI().getPlotById(new BukkitPlayer(player));					
					
					int plotVotes = 0;
					if(instance.getPlotVotes().get(plot)!=null) plotVotes = instance.getPlotVotes().get(plot);
					
					instance.getPlayerVote().put(player, plot);
					instance.getPlotVotes().put(plot, plotVotes);
					
				} else if(itemName.equals(ChatColor.RED+"Take Vote")){
					Plot plot = instance.getPlayerVote().get(player);
					int votes = instance.getPlotVotes().get(plot);
					instance.getPlotVotes().put(plot, votes-1);
					instance.getPlayerVote().put(player, null);
				}
			}
		}
	}
	
	@EventHandler(priority=EventPriority.HIGHEST)
	public void onBlockBreak(BlockBreakEvent event){
		if(!SynergyEvent.getStatus().equals(SynergyStatus.STARTED)) event.setCancelled(true);
	}
	@EventHandler(priority=EventPriority.HIGHEST)
	public void onBlockPlace(BlockPlaceEvent event){
		if(!SynergyEvent.getStatus().equals(SynergyStatus.STARTED)) event.setCancelled(true);
	}
	
	@EventHandler(priority=EventPriority.HIGHEST)
	public void onWorldChange(PlayerChangedWorldEvent event){
		loadInventory(event.getPlayer());
	}
	@EventHandler(priority=EventPriority.HIGHEST)
	public void onPlayerJoin(PlayerJoinEvent event){
		loadInventory(event.getPlayer());
	}
	
	public void loadInventory(Player player){
		if(SynergyEvent.getStatus().equals(SynergyStatus.VOTE)){
			player.getInventory().clear();
			SynergyEvent instance = SynergyEvent.getInstance(player);
			ItemStack item;
			if(instance.getPlayerVote().get(player)!=null){
				item = new Wool(DyeColor.RED).toItemStack(1);
				MetaUtils.addName(item, ChatColor.GREEN+"Take Vote");
			} else {
				item = new Wool(DyeColor.LIME).toItemStack(1);
				MetaUtils.addName(item, ChatColor.GREEN+"Vote!");
			}
			player.getInventory().setItem(8, item);
			item = new ItemStack(Material.BOOK);
			MetaUtils.addName(item, ChatColor.DARK_BLUE+"Top Plots");
			player.getInventory().setItem(0, item);
		}
	}
}
