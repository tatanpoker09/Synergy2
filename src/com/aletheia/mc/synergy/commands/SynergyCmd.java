package com.aletheia.mc.synergy.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import com.aletheia.mc.synergy.event.SynergyEvent;
import com.aletheia.mc.synergy.event.SynergyStatus;
import com.aletheia.mc.synergy.utils.Timer;

public class SynergyCmd implements CommandExecutor{
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
		if(args.length<1){
			help(sender);
			return false;
		}
		if(args[0].equals("reload")){
			SynergyEvent.restart();
			return true;
		} else if(args[0].equals("start")){
			if(SynergyEvent.getStatus().equals(SynergyStatus.PRE_START)){
				int days = 14;
				int hours = 0;
				try{
					if(args.length>1){
						days = Integer.parseInt(args[1]);
					}
					if(args.length>2){
						hours = Integer.parseInt(args[2]);
					}
				} catch(NumberFormatException e){
					sender.sendMessage(ChatColor.RED+"You must input a number.");
					return false;
				}
				SynergyEvent.start(days, hours);
				return true;
			} else {
				sender.sendMessage(ChatColor.RED+"You can't start the synergy at this moment.");
				return false;
			}
		} else if(args[0].equals("end")){
			SynergyEvent.end();
			return true;
		} else if(args[0].equals("settimer")){
			if(args.length>1){
				int days = 0;
				int hours = 0;
				try {
					days = Integer.parseInt(args[1]);
					if(args.length>2){
						hours = Integer.parseInt(args[2]);
					} 
				} catch(NumberFormatException exception){
					sender.sendMessage(ChatColor.RED+"You must input a number.");
					return false;
				}
				Timer.startCountdown(days, hours);
				return true;
			} else {
				help(sender);
				return false;
			}
		}
		return false;
	}

	private void help(CommandSender sender){
		sender.sendMessage(ChatColor.RED+"/synergy reload");
		sender.sendMessage(ChatColor.RED+"/synergy start <Days> <Hours>");
		sender.sendMessage(ChatColor.RED+"/synergy end");
		sender.sendMessage(ChatColor.RED+"/synergy settimer {Days} <Hours>");
	}
}
